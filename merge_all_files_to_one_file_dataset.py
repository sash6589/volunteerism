files = ['demo_facebook.csv',
         'liwc_facebook.csv',
         'post_behavior_facebook.csv',
         'user_topic_facebook.csv',
         'contextual_topic_twitter.csv',
         'ego_behavior_twitter.csv',
         'liwc_twitter.csv',
         'post_behavior_twitter.csv',
         'user_topic_twitter.csv',
         'demo_linkedin.csv',
         'post_behavior_linkedin.csv',
         'user_topic_linkedin.csv']

arrs = ['' for _ in range(2500)]

arrs[0] += 'id,'
for i in range(1, 2500):
    arrs[i] += str(i) + ','

dir_path = 'dataset/'
for file_name in files:
    i = 0
    for line in open(dir_path + file_name, 'r'):
        arrs[i] += line[line.index(',')+1:len(line) - 1] + ','
        i += 1

with open('dataset/dataset_missing_data.csv', 'w') as f:
    for line in arrs:
        f.write(line[:len(line) - 1]+'\n')
