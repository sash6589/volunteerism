def make_pairs(arr):
    pairs = []
    prev_ind = 0
    for ind in range(1, len(arr)):
        if arr[ind] - arr[ind-1] > 1:
            pairs.append((arr[prev_ind], arr[ind-1]))
            prev_ind = ind

    pairs.append((arr[prev_ind], arr[len(arr) - 1]))

    return pairs


def find_ranges(file_name):
    ind = 0
    indexes = []
    for line in open(file_name, 'r'):
        if line.find('?') != -1:
            indexes.append(ind)
        ind += 1

    return make_pairs(indexes)


print(find_ranges('dataset/dataset_linkedin_missing_data.csv'))
