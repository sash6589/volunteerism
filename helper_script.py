TWITTER = 1
FACEBOOK = 2
LINKEDIN = 3

CLASS_12 = 4
CLASS_123 = 5
CLASS_13 = 6
CLASS_1 = 7


def get_data_from_file(file):
    ans = []
    for line in open(file, 'r'):
        ans.append(line)
    return ans


def load_X_Ci_s_from_file(ci, s):
    def get_ans(cl, data):
        ans = [data[0]]
        for ind in cl:
            ans.append(data[int(ind)])
        return ans

    if ci == CLASS_12:
        class_12 = get_data_from_file('dataset/class_12.txt')
        if s == TWITTER:
            twitter_data = get_data_from_file('dataset/dataset_twitter_missing_data.csv')
            res = get_ans(class_12, twitter_data)
            return res
        if s == FACEBOOK:
            facebook_data = get_data_from_file('dataset/dataset_facebook_missing_data.csv')
            res = get_ans(class_12, facebook_data)
            return res
        raise Exception("Class doesn't contain this social network")

    if ci == CLASS_123:
        class_123 = get_data_from_file('dataset/class_123.txt')
        if s == TWITTER:
            twitter_data = get_data_from_file('dataset/dataset_twitter_missing_data.csv')
            res = get_ans(class_123, twitter_data)
            return res
        if s == FACEBOOK:
            facebook_data = get_data_from_file('dataset/dataset_facebook_missing_data.csv')
            res = get_ans(class_123, facebook_data)
            return res
        if s == LINKEDIN:
            linkedin_data = get_data_from_file('dataset/dataset_linkedin_missing_data.csv')
            res = get_ans(class_123, linkedin_data)
            return res
        raise Exception("Class doesn't contain this social network")

    if ci == CLASS_13:
        class_13 = get_data_from_file('dataset/class_13.txt')
        if s == TWITTER:
            twitter_data = get_data_from_file('dataset/dataset_twitter_missing_data.csv')
            res = get_ans(class_13, twitter_data)
            return res
        if s == LINKEDIN:
            linkedin_data = get_data_from_file('dataset/dataset_linkedin_missing_data.csv')
            res = get_ans(class_13, linkedin_data)
            return res
        raise Exception("Class doesn't contain this social network")

    if ci == CLASS_1:
        class_1 = get_data_from_file('dataset/class_1.txt')
        if s == TWITTER:
            twitter_data = get_data_from_file('dataset/dataset_twitter_missing_data.csv')
            res = get_ans(class_1, twitter_data)
            return res
        raise Exception("Class doesn't contain this social network")

    raise Exception("This class ci doesn't exists")
