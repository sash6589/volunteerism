def get_data(file):
    ans = []
    for line in open(file, 'r'):
        ans.append(line)
    return ans


def write_class_to_file(c, f):
    with open(f, 'w') as file:
        for num in c:
            file.write(str(num) + '\n')


twitter_data = get_data('dataset/dataset_twitter_missing_data.csv')
facebook_data = get_data('dataset/dataset_facebook_missing_data.csv')
linkedin_data = get_data('dataset/dataset_linkedin_missing_data.csv')

class_12 = []
class_123 = []
class_13 = []
class_1 = []

for i in range(1, 2500):
    if ('?' not in twitter_data[i]) and ('?' not in facebook_data[i]) and ('?' in linkedin_data[i]):
        class_12.append(i)
        continue
    if ('?' not in twitter_data[i]) and ('?' not in facebook_data[i]) and ('?' not in linkedin_data[i]):
        class_123.append(i)
        continue
    if ('?' not in twitter_data[i]) and ('?' in facebook_data[i]) and ('?' not in linkedin_data[i]):
        class_13.append(i)
        continue
    if ('?' not in twitter_data[i]) and ('?' in facebook_data[i]) and ('?' in linkedin_data[i]):
        class_1.append(i)
        continue
    raise Exception("lol wat")

write_class_to_file(class_12, 'dataset/class_12.txt')
write_class_to_file(class_123, 'dataset/class_123.txt')
write_class_to_file(class_13, 'dataset/class_13.txt')
write_class_to_file(class_1, 'dataset/class_1.txt')

